"use client"
import React, { useState } from 'react';
import UpdateBusModal from '@/components/bus/UpdateBusModal';
import DeleteBusModal from '@/components/bus/DeleteBusModal';
import AddBusModal from '@/components/bus/AddBusModal';
import { useGetBusesQuery } from '@/redux/services/busApi';
import AddBus from '@/components/bus/AddBus';

function Page() {
  const { data, error, isLoading } = useGetBusesQuery(null);
  const [search, setSearch] = useState<string>('');

  return (
    <div className="w-screen h-screen bg-white flex flex-col p-2 gap-2">
      <section className="w-full flex h-1/6 py-0 bg-white rounded-xl shadow-xl shadow-slate-400 text-black flex flex-col justify-center items-center">
        <h2 className="text-center">Bus Panel</h2>
        <div className="flex gap-2 px-2">
          <input
            type="text"
            name="search"
            id="search"
            onChange={(event) => {
              setSearch(event.target.value);
            }}
            placeholder="Buscar"
            className="border-4 rounded-lg"
          />
          <AddBusModal />
          <AddBus></AddBus>
        </div>
      </section>

      <section className="flex h-5/6 bg-gray-100 flex-col overflow-x-auto p-2 rounded-xl border-2 border-gray-300 text-black shadow-xl shadow-slate-400">
        <table className="w-full bg-gray-800 text-white rounded-lg">
          <thead>
            <tr>
              <th className="px-4 py-2 text-left w-1/6">ID</th>
              <th className="px-4 py-2 text-left w-1/6">License Plate</th>
              <th className="px-4 py-2 text-left w-1/6">Brand</th>
              <th className="px-4 py-2 text-left w-1/6">Model</th>
              <th className="px-4 py-2 text-left w-1/6">Seats</th>
              <th className="px-4 py-2 text-center w-1/6">Options</th>
            </tr>
          </thead>
          </table>
       
            {isLoading && <p> Is loading ...</p>}
            {error && <p>Some error</p>}
            {!isLoading && !error && data.length === 0 && <p>Data is not available</p>}
            {!isLoading && !error && <div className='table-auto table-content-container max-h-[55vh] overflow-y-auto'>
            <table className="w-full bg-white text-black ">
            <tbody>
            { 
             data.map((bus) => (
              <tr key={bus.id} className='hover:bg-gray-300' >
                <td className="px-4 py-2 text-left w-1/6">{bus.id}</td>
                <td className="px-4 py-2 text-left w-1/6">{bus.licensePlate}</td>
                <td className="px-4 py-2 text-left w-1/6">{bus.brand}</td>
                <td className="px-4 py-2 text-left w-1/6">{bus.model}</td>
                <td className="px-4 py-2 text-left w-1/6">{bus.seats}</td>
                <td className="px-4 py-2 text-left w-1/6">
                  <div className="flex justify-center gap-4 items-center">
                    <UpdateBusModal bus={bus} />
                    <DeleteBusModal bus={bus} />
                  </div>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
        </div>
        }
      </section>
    </div>
  );
}

export default Page;

/*
 useEffect(()=>{
    async function fetchData() {
        try {
          const response = await fetch( search.trim() != "" ?
            `http://localhost:8080/bus/search/${search}`: `http://localhost:8080/bus`
          );
          if (response.ok) {
            const res = await response.json();
            setBuses(res);
            setError(null);
            setIsLoading(false);
          } else {
            setError("Hubo un error al obtener el listado");
          }
        } catch (error) {
          setError("No pudimos hacer la solicitud para obtener el listado de buses");
        }
      }
        fetchData(); 
  },[search])
*/
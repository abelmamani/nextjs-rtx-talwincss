"use client"
import React, { useState } from 'react';
import UpdateBusModal from '@/components/bus/UpdateBusModal';
import DeleteBusModal from '@/components/bus/DeleteBusModal';
import AddBusModal from '@/components/bus/AddBusModal';
import { useGetBusesQuery } from '@/redux/services/busApi';
import AddBusRouteModal from '@/components/bus_route/AddBusRouteModal';
import { useGetBusRoutesQuery } from '@/redux/services/busRouteApi';
import BusRouteCard from '@/components/bus_route/BusRouteCard';

function Page() {
    const {data, isLoading, error} = useGetBusRoutesQuery(null);
    const [search, setSearch] = useState<string>('');
  
    return (
    <article className="w-screen h-screen bg-white flex flex-col gap-2">
        <section className="w-full flex h-[15vh] bg-gray-100 rounded-xl text-black flex flex-col justify-center items-center">
            <h2 className="text-center">Bus Route Panel</h2>
            <div className="flex gap-2 px-2">
            <input
                type="text"
                name="search"
                id="search"
                onChange={(event) => {
                setSearch(event.target.value);
                }}
                placeholder="Buscar"
                className="border-4 rounded-lg"
            />
            <AddBusRouteModal />
            </div>
        </section>
        {isLoading && <p> Is loading ...</p>}
        {error && <p>Some error</p>}
        {!isLoading && !error && data.length === 0 && <p>Data is not available</p>}
        {!isLoading && !error && data?.length != 0 && <section className="w-full h-[85vh] overflow-y-auto bg-gray-100 text-black p-2 rounded-xl grid  xl:grid-cols-3 lg:grid-cols-2 md:grid-cols-2 table-content-container gap-4">
          
          {data.map((busRoute) => (
            <BusRouteCard key={busRoute.id} busRoute={busRoute} />
          ))
          }
        </section>}
    </article>
  );
}

export default Page;
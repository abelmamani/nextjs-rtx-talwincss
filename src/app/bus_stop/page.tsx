"use client"
import React, { useEffect, useState } from 'react';
import AddBusStopModal from '@/components/bus_stop/AddBusStopModal';
import { BusStop, useGetBusStopsQuery } from '@/redux/services/busStopApi';
import UpdateBusStopModal from '@/components/bus_stop/UpdateBusStopModal';
import DeleteBusStopModal from '@/components/bus_stop/DeleteBusStopModal';
import MapComponent from '@/components/bus_stop/MapComponent';
import '../../styles/estilos.css'


function Page() {
  const { data, error, isLoading } = useGetBusStopsQuery();
  const [search, setSearch] = useState<string>('');
  const [center, setCenter] = useState<BusStop | null>(null);

  const handleMarkerClick = (parada: BusStop) => {
    setCenter(parada);
  };
  return (
    <article className="w-screen h-screen bg-white flex flex-col px-2 gap-2">
      <section className="w-full flex h-1/6 py-0 bg-gray-100 rounded-xl border-2 border-gray-300 text-black flex flex-col justify-center items-center">
        <h2 className="text-center">Bus Stop Panel</h2>
        <div className="flex gap-2 px-2">
          <input
            type="text"
            name="search"
            id="search"
            onChange={(event) => {
              setSearch(event.target.value);
            }}
            placeholder="Buscar"
            className="border-4 rounded-lg"
          />
          <AddBusStopModal/>
        </div>
      </section>

      <section className='w-full h-5/6 flex bg-white gap-2 p-2'>
      <div className=" w-full bg-gray-100 flex flex-col overflow-x-auto p-2 rounded-xl border-2 border-gray-300 text-black">
        <table className="w-full bg-gray-800 text-white rounded-lg">
          <thead>
            <tr>
              <th className="px-4 py-2 text-left w-1/2">Name</th>
              <th className="px-4 py-2 text-left w-1/2">Actions</th>
              
            </tr>
          </thead>
          </table>
       
            {isLoading && <p> Is loading ...</p>}
            {error && <p>Some error</p>}
            {!isLoading && !error && data.length === 0 && <p>Data is not available</p>}
            {!isLoading && !error && <div className='table-auto table-content-container max-h-[55vh] overflow-y-auto'>
            <table className="w-full bg-white text-black ">
            <tbody>
            { 
             data.map((item) => (
              <tr key={item.id} className='hover:bg-gray-300' onClick={() => handleMarkerClick(item)}>
                <td className="px-4 py-2 text-left w-1/2">{item.name}</td>
                <td className="px-4 py-2 w-1/2 flex justify-center gap-4 items-center">
                  <UpdateBusStopModal busStop = {item}/>
                  <DeleteBusStopModal busStop = {item}/>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
        </div>
        }
        </div>
        <section className='w-full h-full'>
        <MapComponent paradas = {data} setParada = {setCenter} center = {center} />
        </section>
       
      </section>
    </article>
  );
}

export default Page;
"use client"
import React, { useState } from 'react';
import AddConductorModal from '@/components/conductor/AddConductorModal';
import UpdateConductorModal from '@/components/conductor/UpdateConductorModal';
import DeleteConductorModal from '@/components/conductor/DeleteConductorModal';
import { useGetConductorsQuery } from '@/redux/services/conductorApi';


function Page() {
  const { data, error, isLoading } = useGetConductorsQuery(null);
  const [search, setSearch] = useState<string>('');

  return (
    <div className="w-full h-full bg-white flex flex-col p-2 gap-2">
      <section className="flex h-1/6 py-0 bg-white text-black flex flex-col justify-center items-center shadow-inner shadow-slate-400 rounded-2xl">
        <h2 className="text-center">Conductor Panel</h2>
        <div className="flex gap-2 px-2">
          <input
            type="text"
            name="search"
            id="search"
            onChange={(event) => {
              setSearch(event.target.value);
            }}
            placeholder="Buscar"
            className="border-4 rounded-lg"
          />
          <AddConductorModal/>
        </div>
      </section>

      <section className="shadow-inner shadow-slate-400 rounded-2xl flex h-5/6 bg-gray-100 flex-col overflow-x-auto p-2 text-black">
        <table className="w-full bg-gray-800 text-white rounded-lg">
          <thead>
            <tr>
              <th className="px-4 py-2 text-left w-1/5">ID</th>
              <th className="px-4 py-2 text-left w-1/5">Name</th>
              <th className="px-4 py-2 text-left w-1/5">Last Name</th>
              <th className="px-4 py-2 text-left w-1/5">Hire Date</th>
              <th className="px-4 py-2 text-left w-1/5">Actions</th>
            </tr>
          </thead>
          </table>
       
            {isLoading && <p> Is loading ...</p>}
            {error && <p>Some error</p>}
            {!isLoading && !error && data.length === 0 && <p>Data is not available</p>}
            {!isLoading && !error && <div className='table-auto table-content-container max-h-[55vh] overflow-y-auto'>
            <table className="w-full bg-white text-black ">
            <tbody>
            { 
             data.map((conductor) => (
              <tr key={conductor.id} className='hover:bg-gray-300'>
                <td className="px-4 py-2 text-left w-1/5">{conductor.id}</td>
                <td className="px-4 py-2 text-left w-1/5">{conductor.name}</td>
                <td className="px-4 py-2 text-left w-1/5">{conductor.lastName}</td>
                <td className="px-4 py-2 text-left w-1/5">{conductor.hireDate}</td>
                <td className="px-4 py-2 w-1/5 flex justify-center gap-4 items-center">
                <UpdateConductorModal conductor = {conductor} />
                <DeleteConductorModal conductor = {conductor}/>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
        </div>
        }
      </section>
    </div>
  );
}

export default Page;
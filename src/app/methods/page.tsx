"use client"
import {useState} from "react"
import "../../styles/estilos.css";
import { NumeroModel } from "@/models/NumeroModel";
function page() {

  const [semilla, setSemilla] = useState<number>(0);
  const [arreglo, setArreglo] = useState<NumeroModel[]>([]);
  
  const handleSemillaChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const inputValue = event.target.value;
    setSemilla(inputValue !== "" ? parseInt(inputValue) : 0);
  };

  const agregarCeros = (numero: number, longitudDeseada: number): string => {
    const numeroStr:string = numero.toString();
    return "0".repeat(longitudDeseada - numeroStr.length) + numeroStr;
  }
  
  const obtenerDigitosMedios = (cuadrado: string, n: number): string => {
    return cuadrado.substr((n/2), n);
  };

  
  const cuadradoMedio = (event: React.ChangeEvent<HTMLInputElement>) =>{
    event.preventDefault()
    if(semilla > 0 && semilla.toString().length % 2 == 0){
      setArreglo([]);
      let longitud: number = semilla.toString().length;
      let arregloNuevo: NumeroModel[] = [];
      let i = 1;
      let formateado: string = agregarCeros(semilla*semilla, 2*longitud);
      let actual:string = obtenerDigitosMedios(formateado, longitud);
      while(!arregloNuevo.some(num => num.number == actual)){
        arregloNuevo = [...arregloNuevo, new NumeroModel(i, actual, "0,"+actual)];
        formateado = agregarCeros(parseInt(actual)*parseInt(actual), 2*longitud);
        actual = obtenerDigitosMedios(formateado, longitud);
        i++;
      }
      setArreglo(arregloNuevo);
    }else{
      alert("la semilla debe ser mayor a cero o tener digitos par")
    }
  }
  

  return (
     <div className="w-full h-screen m-auto bg-white flex flex-col items-center">
   
     <form className=" w-2/5 bg-black flex flex-col p-8 mt-20 rounded-3xl">
      <h2 className="text-white text-center text-bold text-2xl ">Metodo del Cuadrado Medio</h2>
     <input className="text-lg p-2 mt-5"
       type="number"
       onChange={handleSemillaChange}
       placeholder="Ingrese la semilla"
     />
    
     <button className="mt-10 bg-green-600 text-white p-2 rounded-lg" onClick={cuadradoMedio}>Calcular</button>
     </form>
     
     <div className="w-5/6 p-6 bg-black mt-6 rounded-xl">
      <h2 className="text-white text-2xl mb-4 text-center">Resultados</h2>
      <div className="overflow-x-auto">
        <table className="w-full bg-black text-white">
          <thead>
            <tr>
              <th className="px-4 py-2 text-left w-1/3">N</th>
              <th className="px-4 py-2 text-left w-1/3">Numero</th>
              <th className="px-4 py-2 text-left w-1/3">Aleatorio</th>
            </tr>
          </thead>
        </table>
        <div className="table-content-container max-h-[35vh] overflow-y-auto">
          <table className="w-full bg-gray-800 text-white table-auto">
            <tbody>
              {arreglo.map((item) => (
                <tr key={item.id} className="bg-black text-white text-bold p-5">
                  <td className="px-4 py-2 text-left w-1/3">{item.id}</td>
                  <td className="px-4 py-2 text-left w-1/3">{item.number}</td>
                  <td className="px-4 py-2 text-left w-1/3">{item.aleatorio}</td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
  )
}

export default page

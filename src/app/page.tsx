"use client"
import React from 'react';
import Grafico from '@/components/dashboard/Grafico';
import PieChartExample from '../components/dashboard/PieChartExample';

export default function Home() {
  return (
    <main className='w-screen h-screen bg-white p-4'>
        <div className='w-full h-1/2'>
          <h1 className='text-center text-bold md:text-3xl lg:text-6xl'>Welcome to Next 13</h1>
          <div className='w-full h-1/2 flex justify-around mt-8'>
            <PieChartExample></PieChartExample>
            <PieChartExample></PieChartExample>
            <PieChartExample></PieChartExample>
          </div>
         
        </div>
        <div className='w-full h-1/2 flex justify-around'>
          <Grafico></Grafico>
          <Grafico></Grafico>
        </div>
      
       

     
    </main>
  )
}


import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus, faTimes } from '@fortawesome/free-solid-svg-icons';

interface AddModalProps {
  title: string;
  onSubmit: (formData: any) => void;
  isOpen: boolean; // Variable de estado isOpen
  setIsOpen: React.Dispatch<React.SetStateAction<boolean>>;
  children: React.ReactNode;
}

const AddModal: React.FC<AddModalProps> = ({ title, onSubmit, isOpen, setIsOpen, children }) => {

  const openModal = () => {
    setIsOpen(true);
  };

  const closeModal = () => {
    setIsOpen(false);
  };

  const handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    const formData = Object.fromEntries(new FormData(event.currentTarget));
    onSubmit(formData);
  };

  return (
    <div className="relative">
      <button onClick={openModal} className="bg-green-600 text-white text-bold border-2 py-2 px-4 rounded-lg flex justify-center items-center gap-2 ">
        {`Add ${title}`} <FontAwesomeIcon icon={faPlus} className="mr-2 text-lg" />
      </button>
      {isOpen && (
        <div className="fixed top-0 left-0 w-full h-full flex justify-center items-center z-20">
          <div className="fixed inset-0 bg-black opacity-50"></div>
          <div className="bg-white p-4 rounded shadow-md z-10 relative">
            <button onClick={closeModal} className="absolute top-4 right-4 text-black text-xl">
              <FontAwesomeIcon icon={faTimes} />
            </button>
            <h2 className="text-2xl mb-4 text-center text-bold">Add a new {title}</h2>
            <form onSubmit={handleSubmit} className="bg-white flex flex-col gap-4 justify-center mt-4 px-2 py-2">
              {children}
              <button type="submit" className="bg-black text-bold text-white py-2 px-4 rounded-lg text-md">
                Save
              </button>
            </form>
          </div>
        </div>
      )}
    </div>
  );
};

export default AddModal;

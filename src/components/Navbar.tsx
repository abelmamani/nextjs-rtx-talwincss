import Link from "next/link"

function Navbar() {
  return (
    <nav className="w-full h-14 bg-black flex justify-around items-center fixed">
      <Link href={"/"} className="text-white text-2xl text-bold">CainMac</Link>
      <ul className="flex gap-10">
        <li className="">
          <Link href={"/"} className="text-white text-sm text-bold">Home</Link>
        </li>
        <li>
          <Link href={"/bus"} className="text-white text-sm text-bold">Buses</Link>
        </li>
        <li>
          <Link href={"/methods"} className="text-white text-sm text-bold">Metodos</Link>
        </li>
      </ul>
    </nav>
  )
}

export default Navbar

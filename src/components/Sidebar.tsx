"use client"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBars, faHome, faBus, faCog } from '@fortawesome/free-solid-svg-icons';
import {faMapSigns, faUser, faRoute } from '@fortawesome/free-solid-svg-icons';
import { faChartBar } from '@fortawesome/free-solid-svg-icons';
import { faSignOutAlt } from '@fortawesome/free-solid-svg-icons';


import Link from 'next/link';
import React, { useState, ReactNode } from 'react';
import "../styles/estilos.css"

interface SidebarProps {
  children: ReactNode;
}

const Sidebar = ({ children}: SidebarProps) => {
  const [isOpen, setIsOpen] = useState(false);

  const toggleSidebar = () => {
    setIsOpen(!isOpen);
  };

  return (
    <div className="flex w-screen h-screen bg-gray-100">
      <div
        className={`bg-black text-white flex-shrink-0 relative ${
          isOpen ? 'w-64' : 'w-14'
        }`}
      >
        <button
          className={`bg-black text-white px-4 py-2 absolute ${isOpen ? 'top-3 right-0' : 'top-2 left-0'}`}
          onClick={toggleSidebar}
        >
          <FontAwesomeIcon
            icon={faBars}
            className={`text-2xl hover:rotate-180 hover:duration-700`}
          />
        </button>

        <div className={`p-4`}>
          <h2 className="text-2xl font-bold">{isOpen ? 'CainMac' : ''}</h2>
        </div>

        <nav className="flex flex-col py-2">
          <Link href="/" className="px-4 py-5 link">
            <FontAwesomeIcon icon={faHome} className="mr-2 text-xl icono" />
            {isOpen ? 'Home': ''}
          </Link>
          <Link href="/bus" className="px-4 py-5 link">
            <FontAwesomeIcon icon={faBus} className="mr-2 text-xl icono" />
            {isOpen ? 'Bus': ''}
          </Link>

          <Link href="/bus_stop" className="px-4 py-5 link">
          <FontAwesomeIcon icon={faMapSigns}  className="mr-2 text-xl icono" />
            {isOpen ? 'Bus Stop': ''}
          </Link>

          <Link href="/conductor" className="px-4 py-5 link">
          <FontAwesomeIcon icon={faUser}  className="mr-2 text-xl icono" />
            {isOpen ? 'Conductor': ''}
          </Link>

          <Link href="/bus_route" className="px-4 py-5 link">
          <FontAwesomeIcon icon={faRoute}  className="mr-2 text-xl icono" />
            {isOpen ? 'Bus Route': ''}
          </Link>

          <Link href="/methods" className="px-4 py-5 link">
            <FontAwesomeIcon icon={faCog} className="mr-2 text-xl icono" />
            {isOpen ? 'Settings': ''}    
          </Link>
          <Link href="/methods" className="px-4 py-5 link">
            <FontAwesomeIcon icon={faChartBar} className="mr-2 text-xl icono" />
            {isOpen ? 'About': ''}    
          </Link>
          <Link href="/methods" className="px-4 py-5 link mt-5">
            <FontAwesomeIcon icon={faSignOutAlt} className="mr-2 text-xl icono" />
            {isOpen ? 'Log out': ''}    
          </Link>
      
        </nav>
      </div>

      {/* Contenido */}
    
 
        {children}
     
    </div>
  );
};

export default Sidebar;


"use client"
import React, { useState } from 'react';
import { useCreateBusMutation, IBus } from '../../redux/services/busApi';
import AddModal from '../AddModal';
import {toast } from 'react-toastify';

const AddBus: React.FC = () => {
  const [createBus] = useCreateBusMutation();
  const [isOpen, setIsOpen] = useState<boolean>(false);

  const handleSubmit =  (formData: any) => {
    const newBus: IBus = {
      licensePlate: formData.license,
      brand: formData.brand,
      model: formData.model,
      seats: formData.seats,
    };

    createBus(newBus)
      .unwrap()
      .then(() => {
        toast.success('¡Acción exitosa!');
        setIsOpen(false);
      })
      .catch(() => {
        toast.error('Ocurrió un error');
      
      });
  };


  return <AddModal title="Title" onSubmit={handleSubmit} isOpen={isOpen} setIsOpen={setIsOpen}>
    <input type='text' name='license' required placeholder='License Plate' className='bg-white p-2 rounded border-2 border-b-indigo-500'/>
    <input type='text' name='brand'  required  placeholder='Brand' className='bg-white p-2 rounded border-2 border-b-indigo-500'/>
    <input type='text' name='model'  required  placeholder='Model' className='bg-white p-2 rounded border-2 border-b-indigo-500'/>
    <input type='number' name='seats' min="20" max="45" required  placeholder='Seats' className='bg-white p-2 rounded border-2 border-b-indigo-500'/>
  </AddModal>;
};

export default AddBus;

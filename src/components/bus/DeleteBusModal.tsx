"use client"
import React, { useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {faTimes, faTrash } from '@fortawesome/free-solid-svg-icons';
import {toast } from 'react-toastify';
import {IBus, useDeleteBusMutation} from '../../redux/services/busApi';


function DeleteBusModal({bus}: IBus) {
  const [deleteBus] = useDeleteBusMutation();
  const [isOpen, setIsOpen] = useState(false);

  const openModal = () => {
    setIsOpen(true);
  };

  const closeModal = () => {
    setIsOpen(false);
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    deleteBus(bus.id)
    .unwrap()
    .then((payload) => {
      toast.success('¡Acción exitosa!');
      closeModal();
    })
    .catch((error) => {
      toast.error("ocurrio un error! ");
    })
  };

  return (
    <div className="relative">
      <button onClick={openModal} className=" rounded px-4 hover:bg-gray-300"><FontAwesomeIcon icon={faTrash} className="mr-2 text-2xl" /> 
      </button>
      {isOpen && (
        <div className="fixed top-0 left-0 w-full h-full flex items-center justify-center z-20">
          <div className="fixed inset-0 bg-black opacity-50"></div>
          <div className="bg-white text-black border-2 p-4 rounded-2xl shadow-md z-10 relative">
            <button onClick={closeModal} className="absolute top-4 right-4 text-xl">
              <FontAwesomeIcon icon={faTimes} />
            </button>
            <h2 className="text-2xl mb-4 text-center text-bold">Delete a Bus</h2>
            <p className='text-md mb-4 text-center'>Estas seguro de que deses eliminar el colectivo {bus.brand} {bus.model} con matricula {bus.licensePlate} ?</p>
            <form  onSubmit={handleSubmit} className="flex gap-4 justify-center mt-4 px-2 py-2 ">
              <button onClick={closeModal} className="bg-black text-white text-bold py-2 px-4 rounded-lg ">No, cancelar</button>
              <button type="submit" className="bg-red-700 text-white text-bold py-2 px-4 rounded-lg">Si, eliminar</button>
            </form>
          </div>
        </div>
      )}
    </div>
  );
}

export default DeleteBusModal;

"use client"
import React, { useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit, faTimes } from '@fortawesome/free-solid-svg-icons';
import {toast } from 'react-toastify';
import {IBus, useUpdateBusMutation} from '../../redux/services/busApi';

function UpdateBusModal({bus}: IBus) {
  const [updateBus] = useUpdateBusMutation();
  const [isOpen, setIsOpen] = useState(false);

  const openModal = () => {
    setIsOpen(true);
  };

  const closeModal = () => {
    setIsOpen(false);
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    const license = event.target.elements.license.value.trim();
    const brand = event.target.elements.brand.value.trim();
    const model = event.target.elements.model.value.trim();
    const seats = event.target.elements.seats.value;
    const newBus: IBus = {
      id: bus.id,
      licensePlate: license,
      brand: brand,
      model: model,
      seats: seats
    }
    updateBus(newBus)
    .unwrap()
    .then((payload) => {
      toast.success('¡Acción exitosa!');
      closeModal();
    })
    .catch((error) => {
      toast.error("ocurrio un error! ");
    })
  };

  return (
    <div className="relative">
      <button onClick={openModal} className="text-bold py-2 px-4 rounded-lg flex justify-center items-center gap-2"><FontAwesomeIcon icon={faEdit} className="mr-2 text-2xl" /> 
      </button>
      {isOpen && (
        <div className="fixed top-0 left-0 w-full h-full flex items-center justify-center z-20">
          <div className="fixed inset-0 bg-black opacity-50"></div>
          <div className="bg-white p-4 rounded shadow-md z-10 relative">
            <button onClick={closeModal} className="absolute top-4 right-4 text-black text-xl">
              <FontAwesomeIcon icon={faTimes} />
            </button>
            <h2 className="text-2xl mb-4 text-center text-bold">Edit a bus</h2>
            <form  onSubmit={handleSubmit} className="bg-white flex flex-col gap-4 justify-center mt-4 px-2 py-2">
            <input type='text' name='license' required defaultValue={bus.licensePlate} placeholder='License Plate' className='bg-white p-2 rounded border-2 border-b-indigo-500'/>
              <input type='text' name='brand'  required  defaultValue={bus.brand} placeholder='Brand' className='bg-white p-2 rounded border-2 border-b-indigo-500'/>
              <input type='text' name='model'  required defaultValue={bus.model}  placeholder='Model' className='bg-white p-2 rounded border-2 border-b-indigo-500'/>
              <input type='number' name='seats' required defaultValue={bus.seats}  placeholder='Seats' className='bg-white p-2 rounded border-2 border-b-indigo-500'/>
              <button type="submit" className="bg-black text-bold text-white py-2 px-4 rounded-lg text-md">Save</button>
            </form>
          </div>
        </div>
      )}
    </div>
  );
}

export default UpdateBusModal;

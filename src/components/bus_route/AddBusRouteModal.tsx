"use client"
import React, { useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus, faTimes } from '@fortawesome/free-solid-svg-icons';
import {toast } from 'react-toastify';
import {IBusTripRequestModel, ICreateBusRouteRequestModel, IRoutePoint, IRoutePointRequestModel, useCreateBusRouteMutation } from '@/redux/services/busRouteApi';
import RoutePoint from '../route_point/RoutePoint';
import BusTrip from '../bus_trip/BusTrip';

function AddBusRouteModal() {
  const [createBusRoute] = useCreateBusRouteMutation();
  const [routePoints, setRoutePoints] = useState<IRoutePointRequestModel[]>([]);
  const [busTrips, setBusTrips] = useState<IBusTripRequestModel[]>([]);
  const [isOpen, setIsOpen] = useState(false);
  
  const openModal = () => {
    setIsOpen(true);
  };

  const closeModal = () => {
    setIsOpen(false);
    setRoutePoints([]);
    setBusTrips([]);
  };

  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    const name = event.target.elements.name.value.trim();
    const color = event.target.elements.color.value.trim();
    const newBusRoute: ICreateBusRouteRequestModel = {
      id: null,
      name,
      color,
      description: "vacio",
      route_points: routePoints,
      bus_trips: busTrips
    }
    createBusRoute(newBusRoute)
    .unwrap()
    .then((payload) => {
      toast.success('¡Acción exitosa!');
      closeModal();
    })
    .catch((error) => {
      toast.error("ocurrio un error! ");
    })
  };

  return (
    <div className="relative">
      <button onClick={openModal} className="bg-gray-800 text-white text-bold border-2 py-2 px-4 rounded-lg flex justify-center items-center gap-2 ">
      Add <FontAwesomeIcon icon={faPlus} className="mr-2 text-lg" /> 
      </button>
      {isOpen && (
        <div className="fixed top-0 left-0 w-full h-full flex justify-center items-center z-20">
          <div className="fixed inset-0 bg-black opacity-50"></div>
          <div className="bg-white p-4 rounded shadow-md z-10 relative">
            <button onClick={closeModal} className="absolute top-4 right-4 text-black text-xl">
              <FontAwesomeIcon icon={faTimes} />
            </button>
            <h2 className="text-2xl mb-4 text-center text-bold">Add a new bus Route</h2>
            <form  onSubmit={handleSubmit} className="bg-white flex flex-col gap-4 mt-4 px-2 py-2">
              <div className='flex gap-2'>
                <input type='text' name='name'  required  placeholder='Name' className='bg-white p-2 rounded border-2 border-b-indigo-500'/>
              
                <input type='color' name='color'  required  placeholder='Color' className='bg-white p-2 rounded border-2 border-b-indigo-500'/>
              </div>
              <div className='flex gap-4'>
                <RoutePoint routePoints = {routePoints} setRoutePoints = {setRoutePoints}></RoutePoint>
                <BusTrip busTrips={ busTrips} setBusTrips={setBusTrips}></BusTrip>
              </div>
            
              <button type="submit" className="bg-black text-bold text-white py-2 px-4 rounded-lg text-md">Save</button>
            </form>
          </div>
        </div>
      )}
    </div>
  );
}

export default AddBusRouteModal;

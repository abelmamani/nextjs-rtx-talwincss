import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {faRoute } from '@fortawesome/free-solid-svg-icons';
import { BusRoute } from '@/redux/services/busRouteApi';
const BusRouteCard = ({ busRoute }: BusRoute) => {
  const cardStyle = {
    backgroundColor: busRoute.color,
  };

  return (
    <div className="bg-white shadow-xl rounded-2xl flex justify-between items-center">
     
      <h3 className="text-xl font-semibold p-4">{busRoute.name}</h3>
      <span className='h-full p-2 rounded-md' style={cardStyle}></span>  
    </div>
  );
};

export default BusRouteCard;


import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBus } from '@fortawesome/free-solid-svg-icons';
import { GoogleMap, LoadScript, Marker } from '@react-google-maps/api';
import { BusStop } from '@/redux/services/busStopApi';
import { useEffect, useState } from 'react';


interface MapComponentProps {
  paradas: BusStop[];
  center: BusStop | null;
}

const containerStyle = {
  width: '100%',
  height: '100%'
};

const MapComponent: React.FC<MapComponentProps> = ({ paradas, center }) => {
  const [mapCenter, setMapCenter] = useState({
    lat: -29.165168,
    lng: -67.496213
  });

  useEffect(() => {
    if (center) {
      setMapCenter({
        lat: parseFloat(center.latitude),
        lng: parseFloat(center.longitude)
      });
    }
  }, [center]);
  return (
      <LoadScript googleMapsApiKey={process.env.NEXT_PUBLIC_REACT_APP_GOOGLE_MAPS_APIKEY}>
      <GoogleMap
       mapContainerStyle={containerStyle}
       center={mapCenter}
        zoom={14}
        options={{
          zoomControl: false,
          streetViewControl: false,
          fullscreenControl: false,
          mapTypeControl: false,
          styles: [
            // Aquí puedes añadir los estilos personalizados
          ]
        }}
      >

         {paradas?.map(item => (
          <Marker key={item.id} position={{ lat: parseFloat(item.latitude), lng: parseFloat(item.longitude) }} />
        ))}
      </GoogleMap>
    </LoadScript>

    
  );
};

export default MapComponent;
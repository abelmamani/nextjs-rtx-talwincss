"use client"
import React, { useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit, faTimes } from '@fortawesome/free-solid-svg-icons';
import {toast } from 'react-toastify';
import {IBusStop, useUpdateBusStopMutation } from '@/redux/services/busStopApi';

function UpdateBusStopModal({busStop}: IBusStop) {
  const [updateBusStop] = useUpdateBusStopMutation();
  const [isOpen, setIsOpen] = useState(false);
  
  const openModal = () => {
    setIsOpen(true);
  };

  const closeModal = () => {
    setIsOpen(false);
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    const id = busStop.id;
    const name = event.target.elements.name.value.trim();
    const description = event.target.elements.description.value.trim();
    const latitude = event.target.elements.latitude.value.trim();
    const longitude = event.target.elements.longitude.value.trim();
   
 
    const newBusStop: IBusStop = {
      id,
      name,
      description,
      latitude,
      longitude
    }

    updateBusStop(newBusStop)
    .unwrap()
    .then((payload) => {
      toast.success('¡Acción exitosa!');
      closeModal();
    })
    .catch((error) => {
      toast.error("ocurrio un error! ");
    })
  };

  return (
    <div className="relative">
      <button onClick={openModal} className="text-bold py-2 px-4 rounded-lg flex justify-center items-center gap-2 ">
      <FontAwesomeIcon icon={faEdit} className="mr-2 text-2xl" /> 
      </button>
      {isOpen && (
        <div className="fixed top-0 left-0 w-full h-full flex justify-center items-center z-20">
          <div className="fixed inset-0 bg-black opacity-50"></div>
          <div className="bg-white p-4 rounded shadow-md z-10 relative">
            <button onClick={closeModal} className="absolute top-4 right-4 text-black text-xl">
              <FontAwesomeIcon icon={faTimes} />
            </button>
            <h2 className="text-2xl mb-4 text-center text-bold">Edita Bus Stop</h2>
            <form  onSubmit={handleSubmit} className="bg-white flex flex-col gap-4 justify-center mt-4 px-2 py-2">
              <input type='text' name='name' required placeholder='Name' defaultValue={busStop.name} className='bg-white p-2 rounded border-2 border-b-indigo-500'/>
              <input type='text' name='description'  required  placeholder='Description' defaultValue={busStop.description} className='bg-white p-2 rounded border-2 border-b-indigo-500'/>
              <input type='text' name='latitude' required placeholder='Latitude' defaultValue={busStop.latitude} assName='bg-white p-2 rounded border-2 border-b-indigo-500'/>
              <input type='text' name='longitude' required placeholder='Longitude' defaultValue={busStop.longitude} className='bg-white p-2 rounded border-2 border-b-indigo-500'/>
             
              <button type="submit" className="bg-black text-bold text-white py-2 px-4 rounded-lg text-md">Save</button>
            </form>
          </div>
        </div>
      )}
    </div>
  );
}

export default UpdateBusStopModal;

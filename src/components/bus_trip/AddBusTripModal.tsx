"use client"
import React, { useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus, faTimes } from '@fortawesome/free-solid-svg-icons';
import { toast } from 'react-toastify';
import { IBusTripRequestModel} from '@/redux/services/busRouteApi';
import { useGetBusesQuery } from '@/redux/services/busApi';
import { useGetConductorsQuery } from '@/redux/services/conductorApi';

interface AddBusTripModalProps {
  busTrips: IBusTripRequestModel[];
  setBusTrips: React.Dispatch<React.SetStateAction<IBusTripRequestModel[]>>;
}


function AddBusTripModal({ busTrips, setBusTrips}: AddBusTripModalProps) {
  const [busTrip, setBusTrip] = useState<IBusTripRequestModel>({id: null,start_time: "00:00", bus_id: null, conductor_id: null});
  const { data: buses} = useGetBusesQuery(null);
  const { data: conductors} = useGetConductorsQuery(null);
  const [isOpen, setIsOpen] = useState(false);

  const openModal = () => {
    setIsOpen(true);
  };

  const closeModal = () => {
    setIsOpen(false);
    limpiar();
  };

  const handleChange = (event: React.ChangeEvent<HTMLInputElement | HTMLSelectElement>) => {
    const { name, value } = event.target;
    setBusTrip({...busTrip, [name]: value});
  };

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    const isValidBus = busTrip.bus_id == null;
    const isValidConductor = busTrip.conductor_id == null;
    if(isValidBus || isValidConductor){
      toast.warning("seleccione un Bus y Conductor");
    }else{
      setBusTrips([...busTrips, busTrip]); // Usar setRoutePoints en lugar de setRoutePoint
      limpiar();
      toast.success("se agrego un nuevo route point!");
      closeModal();
    }
   
  };

  const limpiar = () => {
    setBusTrip({id: null,start_time: "00:00", bus_id: null, conductor_id: null});
  }

  return (
    <div className="relative">
      <button onClick={openModal} className="bg-gray-800 text-white text-bold border-2 py-2 px-4 rounded-lg flex justify-center items-center gap-2 ">
      Add <FontAwesomeIcon icon={faPlus} className="mr-2 text-lg" /> 
      </button>
      {isOpen && (
        <div className="fixed top-0 left-0 w-full h-full flex justify-center items-center z-30">
          <div className="fixed inset-0 bg-black opacity-50"></div>
          <div className="bg-white p-4 rounded shadow-md z-40 relative">
            <button onClick={closeModal} className="absolute top-4 right-4 text-black text-xl">
              <FontAwesomeIcon icon={faTimes} />
            </button>
            <h2 className="text-2xl mb-4 text-center text-bold">Add a new Bus Trip</h2>
            <div className="bg-white flex flex-col gap-4 justify-center mt-4 px-2 py-2">
                <input type='text' name='start_time'  required  placeholder='Start Time' onChange={handleChange} className='bg-white p-2 rounded border-2 border-b-indigo-500'/>
               
                <select id="bus_id" name="bus_id" onChange={handleChange}>
                  <option value="null">Seleccione una Bus</option>
                  {buses?.map((bus) => (
                    <option key={bus.id} value={bus.id}>
                      {`${bus.licensePlate} ${bus.brand} ${bus.model}`}
                    </option>
                  ))}
                </select>
                <select id="conductor_id" name="conductor_id" onChange={handleChange}>
                  <option value="null">Seleccione un Conductor</option>
                  {conductors?.map((conductor) => (
                    <option key={conductor.id} value={conductor.id}>
                      {`${conductor.name} ${conductor.lastName}`}
                    </option>
                  ))}
                </select>
                <button onClick={handleClick} className="bg-black text-bold text-white py-2 px-4 rounded-lg text-md">Save</button>
            </div>
          </div>
        </div>
      )}
    </div>
  );
}

export default AddBusTripModal;


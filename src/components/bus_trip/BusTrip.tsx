"use client"
import React, { useState } from 'react';
import { IBusTripRequestModel} from '@/redux/services/busRouteApi';
import AddBusTripModal from './AddBusTripModal';

interface AddBusTripModalProps {
  busTrips: IBusTripRequestModel[];
  setBusTrips: React.Dispatch<React.SetStateAction<IBusTripRequestModel[]>>;
}

function BusTrip({ busTrips, setBusTrips }: AddBusTripModalProps) {
  const [search, setSearch] = useState<string>("");
  return (
    <div className="w-full h-full bg-white flex flex-col p-2 gap-2">
      <section className="flex h-1/6 py-0 bg-white text-black flex flex-col justify-center items-center shadow-inner shadow-slate-400 rounded-2xl">
        <h2 className="text-center">Bus Trips Panel Panel</h2>
        <div className="flex gap-2 px-2">
          <input
            type="text"
            name="search"
            id="search"
            onChange={(event) => {
              setSearch(event.target.value);
            }}
            placeholder="Buscar"
            className="border-4 rounded-lg"
          />
          <AddBusTripModal busTrips={busTrips} setBusTrips={setBusTrips} />
        </div>
      </section>

      <section className="shadow-inner shadow-slate-400 rounded-2xl flex h-5/6 bg-gray-100 flex-col overflow-x-auto p-2 text-black">
        <table className="w-full bg-gray-800 text-white rounded-lg">
          <thead>
            <tr>
              <th className="px-4 py-2 text-left w-1/4">Start Time</th>
              <th className="px-4 py-2 text-left w-1/4">Bus</th>
              <th className="px-4 py-2 text-left w-1/4">Conductor</th>
              <th className="px-4 py-2 text-left w-1/4">Options</th>
            </tr>
          </thead>
          </table>
          <div className='table-auto table-content-container max-h-[55vh] overflow-y-auto'>
            <table className="w-full bg-white text-black ">
            <tbody>
            { 
             busTrips.map((trip) => (
              <tr key={busTrips.start_time} className='hover:bg-gray-300'>
                <td className="px-4 py-2 text-left w-1/4">{trip.start_time}</td>
                <td className="px-4 py-2 text-left w-1/4">{trip.bus_id}</td>
                <td className="px-4 py-2 text-left w-1/4">{trip.conductor_id}</td>
                <td className="px-4 py-2 w-1/4 flex justify-center gap-4 items-center">
                    Opciones
                </td>
              </tr>
            ))}
          </tbody>
        </table>
        </div>
        
      </section>
    </div>
  );
}

export default BusTrip;
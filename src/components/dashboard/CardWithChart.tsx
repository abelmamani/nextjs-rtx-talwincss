"use client"
import React from 'react';
import { PieChart, Pie, Cell } from 'recharts';

const COLORS = ['#0088FE', '#00C49F', '#FFBB28', '#FF8042'];

const data = [
  { name: 'A', value: 400 },
  { name: 'B', value: 300 },
  { name: 'C', value: 200 },
  { name: 'D', value: 100 },
];

const CardWithChart = () => {
  return (
    <div className="card bg-gray-200 rounded-lg shadow-md p-4">
      <h3 className="text-lg font-semibold mb-2">Gráfico de Torta</h3>
      <PieChart width={200} height={200}>
        <Pie dataKey="value" data={data} cx={100} cy={100} outerRadius={80} fill="#8884d8">
          {data.map((entry, index) => (
            <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
          ))}
        </Pie>
      </PieChart>
    </div>
  );
};

export default CardWithChart;

"use client"
import React from 'react';
import { LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend } from 'recharts';

const data = [
  { name: 'Jan', valueA: 3000, valueB: 65200, valueC: 14440 },
  { name: 'Feb', valueA: 3400, valueB: 3500, valueC: 63010 },
  { name: 'Mar', valueA: 3200, valueB: 55300, valueC: 74590 },
  { name: 'Apr', valueA: 11500, valueB: 33000, valueC: 13500 },
  { name: 'May', valueA: 3200, valueB: 3300, valueC: 3500 },
  { name: 'Jun', valueA: 45000, valueB: 33000, valueC: 3500 },
  { name: 'Jul', valueA: 3100, valueB: 33000, valueC: 3500 },
  { name: 'Aug', valueA: 31000, valueB: 3000, valueC: 3500 },
  { name: 'Sep', valueA: 3100, valueB: 3300, valueC: 3500 },
  { name: 'Oct', valueA: 3100, valueB: 33000, valueC: 3500 },
  { name: 'Nov', valueA: 43100, valueB: 3300, valueC: 3500 },
  { name: 'Dec', valueA: 3100, valueB: 33000, valueC: 3500 },
  // ... more data ...
];

const Grafico = () => {
  return (
    <div className="flex justify-center">
    <LineChart width={700} height={400} data={data} aspect={2}>
        <XAxis dataKey="name" />
        <YAxis width={2} />
        <CartesianGrid strokeDasharray="3 3" strokeWidth={3} />
        <Tooltip />
        <Legend />
        <Line type="monotone" dataKey="valueA" stroke="#82ca9d" strokeWidth={5} />
      </LineChart>
    </div>
  );
};

export default Grafico;

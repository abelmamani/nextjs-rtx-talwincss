"use client"
import React from 'react';
import { PieChart, Pie, Cell, Legend, Tooltip } from 'recharts';
import CardWithChart from './CardWithChart';

const data = [
  { name: 'A', value: 400 },
  { name: 'B', value: 300 },
  { name: 'C', value: 200 },
  { name: 'D', value: 100 },
];

const COLORS = ['bg-blue-200', 'bg-green-800', 'bg-yellow-800', 'bg-red-500'];

const PieChartExample = () => {
  return (
    <div className="w-full mx-auto">
      <div className="flex flex-col items-center">
        <h3 className="text-lg font-semibold mb-2">Gráfico de Torta</h3>
        <PieChart width={200} height={200}>
          <Pie
            dataKey="value"
            data={data}
            cx={100}
            cy={100}
            outerRadius={80}
            fill="#8884d8"
            label
          >
            {data.map((entry, index) => (
              <Cell
                key={`cell-${index}`}
                className={`p-2 rounded-full ${COLORS[index % COLORS.length]}`}
              />
            ))}
          </Pie>
     
         
          <Tooltip />
       
        </PieChart>
      
      </div>
    </div>
  );
};

export default PieChartExample;

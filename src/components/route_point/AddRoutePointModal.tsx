"use client"
import React, { useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus, faTimes } from '@fortawesome/free-solid-svg-icons';
import { toast } from 'react-toastify';
import { useGetBusStopsQuery } from '@/redux/services/busStopApi';
import { IRoutePointRequestModel } from '@/redux/services/busRouteApi';

interface AddRoutePointModalProps {
  routePoints: IRoutePointRequestModel[];
  setRoutePoints: React.Dispatch<React.SetStateAction<IRoutePointRequestModel[]>>;
}

function AddRoutePointModal({ routePoints, setRoutePoints }: AddRoutePointModalProps) {
  const [routePoint, setRoutePoint] = useState<IRoutePointRequestModel>({id: null, number: 1,
  minutes: 0, bus_stop_id: null});
  const { data, isLoading, error } = useGetBusStopsQuery(null);
  const [isOpen, setIsOpen] = useState(false);

  const openModal = () => {
    setIsOpen(true);
  };

  const closeModal = () => {
    setIsOpen(false);
    limpiar();
  };

  const handleChange = (event: React.ChangeEvent<HTMLInputElement | HTMLSelectElement>) => {
    const { name, value } = event.target;
    setRoutePoint({ ...routePoint, [name]: value });
  };

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    const isValid = routePoint.bus_stop_id == null;
    if(isValid){
      toast.warning("seleccione un Bus Stop!");
    }else{
      setRoutePoints([...routePoints, routePoint]); // Usar setRoutePoints en lugar de setRoutePoint
      toast.success("se agrego un nuevo route point!");
      limpiar();
      closeModal();
    }
   
  };

  const limpiar = () => {
    setRoutePoint({id: null, number: 1,
      minutes: 0, bus_stop_id: null});
  }

  return (
    <div className="relative">
      <button onClick={openModal} className="bg-gray-800 text-white text-bold border-2 py-2 px-4 rounded-lg flex justify-center items-center gap-2 ">
      Add <FontAwesomeIcon icon={faPlus} className="mr-2 text-lg" /> 
      </button>
      {isOpen && (
        <div className="fixed top-0 left-0 w-full h-full flex justify-center items-center z-30">
          <div className="fixed inset-0 bg-black opacity-50"></div>
          <div className="bg-white px-4 py-12 rounded shadow-md z-40 relative">
            <button onClick={closeModal} className="absolute top-4 right-4 text-black text-xl">
              <FontAwesomeIcon icon={faTimes} />
            </button>
            <h2 className="text-2xl mb-4 text-center text-bold">Add a new Route Point</h2>
            <div className="bg-white flex flex-col gap-4 justify-center mt-4 px-2 py-2">
                <input type='number' name='number'  required  placeholder='Number' onChange={handleChange} className='bg-white p-2 rounded border-2 border-b-indigo-500'/>
                <input type='number' name='minutes'  required  placeholder='Minutes' onChange={handleChange} className='bg-white p-2 rounded border-2 border-b-indigo-500'/>
               
                <select id="bus_stop_id" name="bus_stop_id" onChange={handleChange}>
                  <option value="null">Seleccione una Parada</option>
                  {data?.map((busStop) => (
                    <option key={busStop.id} value={busStop.id}>
                      {busStop.name}
                    </option>
                  ))}
                </select>
                <button onClick={handleClick} className="bg-black text-bold text-white py-2 px-4 rounded-lg text-md">Save</button>
            </div>
          </div>
        </div>
      )}
    </div>
  );
}

export default AddRoutePointModal;


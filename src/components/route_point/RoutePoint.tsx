"use client"
import React, { useState } from 'react';
import AddBusRouteModal from '../bus_route/AddBusRouteModal';
import AddRoutePointModal from './AddRoutePointModal';
import { IRoutePoint, IRoutePointRequestModel } from '@/redux/services/busRouteApi';


interface AddRoutePointModalProps {
  routePoints: IRoutePointRequestModel[];
  setRoutePoints: React.Dispatch<React.SetStateAction<IRoutePointRequestModel[]>>;
}

function RoutePoint({ routePoints, setRoutePoints }: AddRoutePointModalProps) {
  const [search, setSearch] = useState<string>("");
  return (
    <div className="w-full h-full bg-white flex flex-col p-2 gap-2">
      <section className="flex h-1/6 py-0 bg-white text-black flex flex-col justify-center items-center shadow-inner shadow-slate-400 rounded-2xl">
        <h2 className="text-center">Route Point Panel Panel</h2>
        <div className="flex gap-2 px-2">
          <input
            type="text"
            name="search"
            id="search"
            onChange={(event) => {
              setSearch(event.target.value);
            }}
            placeholder="Buscar"
            className="border-4 rounded-lg"
          />
          <AddRoutePointModal routePoints={routePoints} setRoutePoints = {setRoutePoints}/>
        </div>
      </section>

      <section className="shadow-inner shadow-slate-400 rounded-2xl flex w-full h-1/2 bg-gray-100 flex-col overflow-x-auto p-2 text-black">
        <table className="w-full bg-gray-800 text-white rounded-lg">
          <thead>
            <tr>
              <th className="px-4 py-2 text-left w-1/4">Number</th>
              <th className="px-4 py-2 text-left w-1/4">Minutes</th>
              <th className="px-4 py-2 text-left w-1/4">Bus Stop</th>
              <th className="px-4 py-2 text-left w-1/4">Options</th>
            </tr>
          </thead>
          </table>
          <div className='table-auto table-content-container max-h-[55vh] overflow-y-auto'>
            <table className="w-full bg-white text-black ">
            <tbody>
            { 
             routePoints.map((point) => (
              <tr key={point.number} className='hover:bg-gray-300'>
                <td className="px-4 py-2 text-left w-1/4">{point.number}</td>
                <td className="px-4 py-2 text-left w-1/4">{point.minutes}</td>
                <td className="px-4 py-2 text-left w-1/4">{point.bus_stop_id}</td>
                <td className="px-4 py-2 w-1/4 flex justify-center gap-4 items-center">
                    Opciones
                </td>
              </tr>
            ))}
          </tbody>
        </table>
        </div>
        
      </section>
    </div>
  );
}

export default RoutePoint;
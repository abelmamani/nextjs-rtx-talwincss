export class BusModel{
    
    constructor(public id: number, public licensePlate: string, public brand: string, public model: string, public seats: number){

    }
}
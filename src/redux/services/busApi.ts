import {createApi, fetchBaseQuery} from '@reduxjs/toolkit/query/react'

export  interface IBus {
    id: number,
    licensePlate: string,
    brand: string,
    model: string,
    seats: number
}

export  interface IBusPost {
  licensePlate: string,
  brand: string,
  model: string,
  seats: number
}

export type Bus = IBus;
type BusPost = IBus;

export const busApi = createApi({
    reducerPath: 'busApi',
    baseQuery: fetchBaseQuery({ baseUrl: 'http://localhost:8080/'}),
    tagTypes: ["Buss"],
    endpoints: (builder) =>({
        getBuses: builder.query<Bus[], void>({
            query: () => 'bus',
            providesTags: ["Buss"],
         
        }),
        searchBuses: builder.query<Bus[], {search: string}>({
          query: (search) => `bus/search/${search}`
      
        }),
        getBusById: builder.query<Bus, {id: number}>({
            query: ({id}) => `bus/${id}`
        }),
        createBus: builder.mutation<{message: string} , BusPost>({
          query: (newBus) => ({
            url: 'bus',
            method: 'POST',
            body: newBus
          }),
          invalidatesTags: ["Buss"],
          
        }),
        updateBus: builder.mutation< Bus ,Bus>({
          query: (bus) => ({
            url: "bus",
            method: "PUT",
            body: bus
          }), 
          invalidatesTags: ["Buss"],
        }),
        deleteBus: builder.mutation< Bus , number>({
          query: (id) => ({
            url: `bus/${id}`,
            method: "DELETE",
          }), 
          invalidatesTags: ["Buss"],
        })
    })
});

export const {useGetBusesQuery, useGetBusByIdQuery, useCreateBusMutation, useUpdateBusMutation, useDeleteBusMutation, useSearchBusesQuery} = busApi

/*
 useEffect(()=>{
    fetch("http://localhost:8080/bus").
    then( response => {
      if (!response.ok) {
        throw new Error('Network response was not ok');
      }
      return response.json();
    })
    .then(data => {
      const busData: BusModel[] = data.map((bus: any) => {
        return new BusModel(
          bus.id,
          bus.licensePlate,
          bus.brand,
          bus.model,
          bus.seats
        );
      });
      setBuses(busData);
    })
    .catch(error => {
      console.error('Error fetching data:', error);
    });
  }, []);

*/

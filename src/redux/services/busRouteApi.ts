import {createApi, fetchBaseQuery} from '@reduxjs/toolkit/query/react'
import { IBusStop } from './busStopApi'
import { IBus } from './busApi'
export interface IRoutePointRequestModel{
  id: number,
  number: number,
  minutes: number,
  bus_stop_id: number
}
export interface IBusTripRequestModel{
  id: number,
  start_time: string, 
  bus_id: number,
  conductor_id: number
}

export  interface ICreateBusRouteRequestModel {
  id: number,
  name: string,
  color: string,
  description: string,
  route_points: IRoutePointRequestModel[],
  bus_trips: IBusTripRequestModel[]
}

export interface IRoutePoint{
  id: number,
  number: number,
  minutes: number,
  busStop: IBusStop
}
export interface IBusTrip{
  id: number,
  start_time: string, 
  bus: IBus
  conductor: IBus
}

export  interface IBusRoute {
    id: number,
    name: string,
    color: string,
    description: string,
    routePoints: IRoutePoint[],
    busTrips: IBusTrip[]
}



export type BusRoute = IBusRoute;
type CreateBusRouteRequestModel = ICreateBusRouteRequestModel;

export const busRouteApi = createApi({
    reducerPath: 'busRouteApi',
    baseQuery: fetchBaseQuery({ baseUrl: 'http://localhost:8080/'}),
    tagTypes: ["busroutes"],
    endpoints: (builder) =>({
        getBusRoutes: builder.query<BusRoute[], void>({
            query: () => 'bus_route',
            providesTags: ["busroutes"],
         
        }),
    
        createBusRoute: builder.mutation<{message: string}, CreateBusRouteRequestModel>({
          query: (newBusRoute) => ({
            url: 'bus_route',
            method: 'POST',
            body: newBusRoute
          }),
          invalidatesTags: ["busroutes"],
          
        }),
    
    })
});

export const {useGetBusRoutesQuery, useCreateBusRouteMutation} = busRouteApi;


import {createApi, fetchBaseQuery} from "@reduxjs/toolkit/query/react"

export  interface IBusStop {
    id: number,
    name: string,
    description: string,
    longitude: string,
    lastitude: string
}

export  interface ICreateBusStopRequestModel {
  name: string,
  description: string,
  longitude: string,
  lastitude: string
}

export type BusStop = IBusStop;
type CreateBusStopRequestModel = ICreateBusStopRequestModel;

export const busStopApi = createApi({
    reducerPath: 'busStopApi',
    baseQuery: fetchBaseQuery({ baseUrl: 'http://localhost:8080/'}),
    tagTypes: ["busStops"],
    endpoints: (builder) =>({
        getBusStops: builder.query<BusStop[], void>({
            query: () => 'bus_stop',
            providesTags: ["busStops"],
         
        }),
        createBusStop: builder.mutation<{message: string} , CreateBusStopRequestModel>({
          query: (newBusStop) => ({
            url: 'bus_stop',
            method: 'POST',
            body: newBusStop
          }),
          invalidatesTags: ["busStops"],
          
        }),
        updateBusStop: builder.mutation< BusStop , BusStop>({
          query: (busStop) => ({
            url: "bus_stop",
            method: "PUT",
            body: busStop
          }), 
          invalidatesTags: ["busStops"],
        }),
        deleteBusStop: builder.mutation< BusStop , number>({
          query: (id) => ({
            url: `bus_stop/${id}`,
            method: "DELETE",
          }), 
          invalidatesTags: ["busStops"],
        })
    })
});

export const {useGetBusStopsQuery, useCreateBusStopMutation, useUpdateBusStopMutation, useDeleteBusStopMutation} = busStopApi
import {createApi, fetchBaseQuery} from "@reduxjs/toolkit/query/react"

export  interface IConductor {
    id: number,
    name: string,
    lastName: string,
    hireDate: Date
}

export  interface ICreateConductorRequestModel {
    name: string,
    lastName: string,
    hireDate: Date
}

export type Conductor = IConductor;
type CreateConductorRequestModel = ICreateConductorRequestModel;

export const conductorApi = createApi({
    reducerPath: 'conductorApi',
    baseQuery: fetchBaseQuery({ baseUrl: 'http://localhost:8080/'}),
    tagTypes: ["conductors"],
    endpoints: (builder) =>({
        getConductors: builder.query<Conductor[], void>({
            query: () => 'conductor',
            providesTags: ["conductors"],
         
        }),
        createConductor: builder.mutation<{message: string} , CreateConductorRequestModel>({
          query: (newConductor) => ({
            url: 'conductor',
            method: 'POST',
            body: newConductor
          }),
          invalidatesTags: ["conductors"],
          
        }),
        updateConductor: builder.mutation< Conductor , Conductor>({
          query: (conductor) => ({
            url: "conductor",
            method: "PUT",
            body: conductor
          }), 
          invalidatesTags: ["conductors"],
        }),
        deleteConductor: builder.mutation< Conductor , number>({
          query: (id) => ({
            url: `conductor/${id}`,
            method: "DELETE",
          }), 
          invalidatesTags: ["conductors"],
        })
    })
});

export const {useGetConductorsQuery, useCreateConductorMutation, useUpdateConductorMutation, useDeleteConductorMutation} = conductorApi
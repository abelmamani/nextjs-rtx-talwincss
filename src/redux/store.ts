import {configureStore} from '@reduxjs/toolkit'
import {busApi} from './services/busApi'
import {setupListeners} from '@reduxjs/toolkit/dist/query'
import {conductorApi } from './services/conductorApi'
import {busStopApi } from './services/busStopApi'
import { busRouteApi } from './services/busRouteApi'
export const store = configureStore({
    reducer: {
        [busApi.reducerPath]: busApi.reducer,
        [conductorApi.reducerPath]: conductorApi.reducer,
        [busStopApi.reducerPath]: busStopApi.reducer,
        [busRouteApi.reducerPath]: busRouteApi.reducer
    },
    middleware: (getDefaultMiddleware) => getDefaultMiddleware().
    concat([busApi.middleware, conductorApi.middleware, busStopApi.middleware, busRouteApi.middleware])
})

setupListeners(store.dispatch)